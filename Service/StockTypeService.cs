﻿namespace WebsiteTest.Service
{
    using System.Linq;
    using Domain.Entities;
    using Domain.Repositories;
    using Domain.Services;

    public class StockTypeService : IStockTypeService
    {
        private readonly IUoW _uow;

        public StockTypeService(IUoW uow)
        {
            _uow = uow;
        }

        public StockType Create(StockType entity)
        {
            _uow.StockTypes.Add(entity);
            _uow.Commit();
            return entity;
        }

        public IQueryable<StockType> SelectAll()
        {
            return _uow.StockTypes.GetAll();
        }

        public StockType SelectById(int id)
        {
            return _uow.StockTypes.GetById(id);
        }

        public void Update(StockType entity)
        {
            _uow.StockTypes.Update(entity);
            _uow.Commit();
        }

        public void Delete(StockType entity)
        {
            _uow.StockTypes.Delete(entity);
            _uow.Commit();
        }

        public void Delete(int id)
        {
            _uow.StockTypes.Delete(id);
            _uow.Commit();
        }
    }
}