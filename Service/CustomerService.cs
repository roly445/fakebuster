﻿namespace WebsiteTest.Service
{
    using System.Linq;
    using Domain.Entities;
    using Domain.Repositories;
    using Domain.Services;

    public class CustomerService : ICustomerService
    {
        private readonly IUoW _uow;

        public CustomerService(IUoW uow)
        {
            _uow = uow;
        }

        public Customer Create(Customer entity)
        {
            _uow.Customers.Add(entity);
            _uow.Commit();
            return entity;
        }

        public IQueryable<Customer> SelectAll()
        {
            return _uow.Customers.GetAll();
        }

        public Customer SelectById(int id)
        {
            return _uow.Customers.GetById(id);
        }

        public void Update(Customer entity)
        {
            _uow.Customers.Update(entity);
            _uow.Commit();
        }

        public void Delete(Customer entity)
        {
            _uow.Customers.Delete(entity);
            _uow.Commit();
        }

        public void Delete(int id)
        {
            _uow.Customers.Delete(id);
            _uow.Commit();
        }
    }
}