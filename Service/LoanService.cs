﻿namespace WebsiteTest.Service
{
    using System.Linq;
    using Domain.Entities;
    using Domain.Repositories;
    using Domain.Services;

    public class LoanService : ILoanService
    {
        private readonly IUoW _uow;

        public LoanService(IUoW uow)
        {
            _uow = uow;
        }

        public Loan Create(Loan entity)
        {
            _uow.Loans.Add(entity);
            _uow.Commit();
            return entity;
        }

        public IQueryable<Loan> SelectAll()
        {
            return _uow.Loans.GetAll();
        }

        public Loan SelectById(int id)
        {
            return _uow.Loans.GetById(id);
        }

        public void Update(Loan entity)
        {
            _uow.Loans.Update(entity);
            _uow.Commit();
        }

        public void Delete(Loan entity)
        {
            _uow.Loans.Delete(entity);
            _uow.Commit();
        }

        public void Delete(int id)
        {
            _uow.Loans.Delete(id);
            _uow.Commit();
        }
    }
}