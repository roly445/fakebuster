﻿namespace WebsiteTest.Service
{
    using System.Linq;
    using Domain.Entities;
    using Domain.Repositories;
    using Domain.Services;

    public class StockService : IStockService
    {
        private readonly IUoW _uow;

        public StockService(IUoW uow)
        {
            _uow = uow;
        }

        public Stock Create(Stock entity)
        {
            _uow.Stocks.Add(entity);
            _uow.Commit();
            return entity;
        }

        public IQueryable<Stock> SelectAll()
        {
            return _uow.Stocks.GetAll();
        }

        public Stock SelectById(int id)
        {
            return _uow.Stocks.GetById(id);
        }

        public void Update(Stock entity)
        {
            _uow.Stocks.Update(entity);
            _uow.Commit();
        }

        public void Delete(Stock entity)
        {
            _uow.Stocks.Delete(entity);
            _uow.Commit();
        }

        public void Delete(int id)
        {
            _uow.Stocks.Delete(id);
            _uow.Commit();
        }
    }
}