﻿namespace WebsiteTest.Domain.Services
{
    using Entities;

    public interface IStockService : IService<Stock> {}
}