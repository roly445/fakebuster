﻿namespace WebsiteTest.Domain.Services
{
    using Entities;

    public interface IStockTypeService : IService<StockType> {}
}