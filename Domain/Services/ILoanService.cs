﻿namespace WebsiteTest.Domain.Services
{
    using Entities;

    public interface ILoanService : IService<Loan> {}
}