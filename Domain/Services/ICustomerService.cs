﻿namespace WebsiteTest.Domain.Services
{
    using Entities;

    public interface ICustomerService : IService<Customer> {}
}