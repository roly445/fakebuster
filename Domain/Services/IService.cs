﻿namespace WebsiteTest.Domain.Services
{
    using System.Linq;
    using Entities;

    public interface IService<T> where T : BaseEntity
    {
        #region Methods

        T Create(T entity);

        IQueryable<T> SelectAll();
        T SelectById(int id);

        void Update(T entity);

        void Delete(T entity);
        void Delete(int id);

        #endregion
    }
}