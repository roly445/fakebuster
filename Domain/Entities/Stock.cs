namespace WebsiteTest.Domain.Entities
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [KnownType(typeof (Loan))]
    [KnownType(typeof (StockType))]
    public class Stock : BaseEntity
    {
        public string Title { get; set; }
        public int StockTypeId { get; set; }

        public virtual ICollection<Loan> Loans { get; set; }

        public virtual StockType StockType { get; set; }
    }
}