﻿namespace WebsiteTest.Domain.Entities
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [KnownType(typeof (Stock))]
    public class StockType : BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<Stock> Stocks { get; set; }
    }
}