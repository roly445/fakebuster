﻿namespace WebsiteTest.Domain.Entities
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [KnownType(typeof (Loan))]
    public class Customer : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ICollection<Loan> Loans { get; set; }
    }
}