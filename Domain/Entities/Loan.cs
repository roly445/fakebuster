﻿namespace WebsiteTest.Domain.Entities
{
    using System;
    using System.Runtime.Serialization;

    [KnownType(typeof(Customer))]
    [KnownType(typeof(Stock))]
    public class Loan : BaseEntity
    {
        public int CustomerId { get; set; }
        public int StockId { get; set; }
        public DateTime LoanedOut { get; set; }
        public DateTime? Returned { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Stock Stock { get; set; }
    }
}