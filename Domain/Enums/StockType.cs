﻿namespace WebsiteTest.Domain.Enums
{
    public enum StockType
    {
        DVD = 1,
        BlueRay = 2,
        XboxGame = 3,
        PS3Game = 4,
        Videos = 5,
        WiiGames = 6
    }
}