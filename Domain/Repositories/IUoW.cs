﻿namespace WebsiteTest.Domain.Repositories
{
    using Entities;

    public interface IUoW
    {
        IRepository<Customer> Customers { get; }
        IRepository<Loan> Loans { get; }
        IRepository<Stock> Stocks { get; }
        IRepository<StockType> StockTypes { get; }

        #region Methods

        void Commit();

        #endregion
    }
}