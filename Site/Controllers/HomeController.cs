﻿namespace WebsiteTest.Site.Controllers
{
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Domain.Entities;
    using Domain.Services;
    using Models;

    public class HomeController : Controller
    {
        private readonly IStockService _stockService;
        private readonly ICustomerService _customerService;
        private readonly ILoanService _loanService;
        private readonly IStockTypeService _stockTypeService;

        public HomeController(
                IStockService stockService, ICustomerService customerService, ILoanService loanService,
                IStockTypeService stockTypeService)
        {
            _stockService = stockService;
            _customerService = customerService;
            _loanService = loanService;
            _stockTypeService = stockTypeService;
        }

        public ActionResult Index()
        {
            return View(_stockService.SelectAll().ToList());
        }

        #region Loan Item

        public ActionResult LoanItem(int id)
        {
            LoanItemModel loanItemModel = new LoanItemModel();

            Stock stock = _stockService.SelectById(id);

            loanItemModel.StockId = stock.Id;
            loanItemModel.Title = stock.Title;
            loanItemModel.StockType = stock.StockType.Name;

            return View(PopulateLoanItemModel(loanItemModel));
        }

        [HttpPost]
        public ActionResult LoanItem(LoanItemModel loanItemModel)
        {
            if (ModelState.IsValid)
            {
                _loanService.Create(
                        new Loan
                        {
                                CustomerId = loanItemModel.SelectedCustomer,
                                LoanedOut = DateTime.Now,
                                StockId = loanItemModel.StockId
                        });

                return RedirectToAction("Index");
            }
            return View(PopulateLoanItemModel(loanItemModel));
        }

        private LoanItemModel PopulateLoanItemModel(LoanItemModel loanItemModel = null)
        {
            if (loanItemModel == null)
                loanItemModel = new LoanItemModel();

            foreach (Customer customer in _customerService.SelectAll())
            {
                loanItemModel.Customers.Add(
                        new SelectListItem
                        {Text = customer.FirstName + " " + customer.LastName, Value = customer.Id.ToString()});
            }
            return loanItemModel;
        }

        #endregion

        #region Add Item

        public ActionResult AddItem()
        {
            return View(PopulateAddItemModel());
        }

        [HttpPost]
        public ActionResult AddItem(AddItemModel addItemModel)
        {
            if (ModelState.IsValid)
            {
                _stockService.Create(
                        new Stock
                        {
                                Title = addItemModel.Title,
                                StockTypeId = addItemModel.SelectedStockType
                        });
                return RedirectToAction("Index");
            }
            return View(PopulateAddItemModel(addItemModel));
        }

        private AddItemModel PopulateAddItemModel(AddItemModel addItemModel = null)
        {
            if (addItemModel == null)
                addItemModel = new AddItemModel();

            foreach (StockType stockType in _stockTypeService.SelectAll())
            {
                addItemModel.StockTypes.Add(
                        new SelectListItem
                        {
                                Text = stockType.Name,
                                Value = stockType.Id.ToString()
                        });
            }

            return addItemModel;
        }

        #endregion

        public ActionResult ReturnItem(int id)
        {
            Loan loan = _loanService.SelectById(id);
            if (loan == null || loan.Returned != null)
                throw new HttpException(404, "");
            ReturnItemModel returnItemModel = new ReturnItemModel
                                              {
                                                      LoanId = loan.Id,
                                                      Name = loan.Customer.FirstName + " " + loan.Customer.LastName,
                                                      Title = loan.Stock.Title,
                                                      Type = loan.Stock.StockType.Name
                                              };
            return View(returnItemModel);
        }

        [HttpPost]
        public ActionResult ReturnItem(ReturnItemModel returnItemModel)
        {
            Loan loan = _loanService.SelectById(returnItemModel.LoanId);
            loan.Returned = DateTime.Now;
            _loanService.Update(loan);

            return RedirectToAction("Index");
        }
    }
}