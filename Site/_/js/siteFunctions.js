﻿var time;

$(document).ready(function () {
    time = new Date().valueOf();
    window.setInterval(function () {
        $('#timer').html('Time on page: ' + ((new Date().valueOf() - time) / 1000) + ' secs');
    }, 1000);

    $('.expand').click(function () {
        $(this).parents('tr').next('tr').toggle();
    });
})