[assembly: WebActivator.PreApplicationStartMethod(typeof (WebsiteTest.Site.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof (WebsiteTest.Site.App_Start.NinjectWebCommon), "Stop")
]

namespace WebsiteTest.Site.App_Start
{
    using System;
    using System.Web;
    using System.Web.Http;
    using System.Web.Http.Dependencies;
    using Domain.Repositories;
    using Domain.Services;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Syntax;
    using Ninject.Web.Common;
    using Repository;
    using Service;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof (OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof (NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            StandardKernel kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            RegisterServices(kernel);

            GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);

            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ICustomerService>().To<CustomerService>();
            kernel.Bind<ILoanService>().To<LoanService>();
            kernel.Bind<IStockService>().To<StockService>();
            kernel.Bind<IStockTypeService>().To<StockTypeService>();

            //kernel.Bind<IWebSecurity>().To<WebSecurityWrapper>();

            kernel.Bind<IUoW>().To<UoW>().InRequestScope();
            kernel.Bind<RepositoryFactories>().To<RepositoryFactories>().InSingletonScope();
            kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>();

            //#if DEBUG
            //            kernel.Bind<IMessageService>().To<EmailService>();
            //#else
            //            kernel.Bind<IMessageService>().To<QueueService>();
            //#endif

            //FluentValidationModelValidationFactory validationFactory =
            //    (metadata, context, rule, validator) =>
            //    new RemoteFluentValidationPropertyValidator(metadata, context, rule, validator);
            //FluentValidationModelValidatorProvider.Configure(x => x.Add(typeof(RemoteValidator), validationFactory));

            //NinjectValidatorFactory ninjectValidatorFactory = new NinjectValidatorFactory(kernel);
            //FluentValidationModelValidatorProvider validationProvider =
            //    new FluentValidationModelValidatorProvider(ninjectValidatorFactory);
            //validationProvider.Add(typeof(RemoteValidator), validationFactory);

            //ModelValidatorProviders.Providers.Add(validationProvider);
            //DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;

            //AssemblyScanner.FindValidatorsInAssembly(Assembly.GetExecutingAssembly()).
            //                ForEach(match => kernel.Bind(match.InterfaceType).To(match.ValidatorType));
        }
    }

    public class NinjectDependencyScope : IDependencyScope
    {
        private IResolutionRoot resolver;

        public NinjectDependencyScope(IResolutionRoot resolver)
        {
            this.resolver = resolver;
        }

        public object GetService(Type serviceType)
        {
            if (resolver == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");

            return resolver.TryGet(serviceType);
        }

        public System.Collections.Generic.IEnumerable<object> GetServices(Type serviceType)
        {
            if (resolver == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");

            return resolver.GetAll(serviceType);
        }

        public void Dispose()
        {
            IDisposable disposable = resolver as IDisposable;
            if (disposable != null)
                disposable.Dispose();

            resolver = null;
        }
    }

    // This class is the resolver, but it is also the global scope
    // so we derive from NinjectScope.
    public class NinjectDependencyResolver : NinjectDependencyScope, IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernel)
                : base(kernel)
        {
            this.kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectDependencyScope(kernel.BeginBlock());
        }
    }
}