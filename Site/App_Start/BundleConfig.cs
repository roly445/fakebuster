﻿namespace WebsiteTest
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/_/css").Include("~/_/css/normalize.css", "~/_/css/site.css"));
        }
    }
}