﻿namespace WebsiteTest
{
    using System.Web.Mvc;
    using System.Web.Routing;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                    name: "AddItem",
                    url: "AddItem",
                    defaults: new {controller = "Home", action = "AddItem"});

            routes.MapRoute(
                    name: "LoanItem",
                    url: "LoanItem/{id}",
                    defaults: new {controller = "Home", action = "LoanItem", id = UrlParameter.Optional});

            routes.MapRoute(
                    name: "ReturnItem",
                    url: "ReturnItem/{id}",
                    defaults: new {controller = "Home", action = "ReturnItem", id = UrlParameter.Optional});

            routes.MapRoute(
                    name: "Default",
                    url: "{controller}/{action}/{id}",
                    defaults: new {controller = "Home", action = "Index", id = UrlParameter.Optional}
                    );
        }
    }
}