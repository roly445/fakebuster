namespace WebsiteTest.Site.Models
{
    public class ReturnItemModel : BaseModel
    {
        public int LoanId { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}