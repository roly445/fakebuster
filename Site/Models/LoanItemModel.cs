namespace WebsiteTest.Site.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class LoanItemModel : BaseModel
    {
        public LoanItemModel()
        {
            Customers = new List<SelectListItem>();
        }

        public int StockId { get; set; }
        public string Title { get; set; }

        [Display(Name = "Type")]
        public string StockType { get; set; }

        public List<SelectListItem> Customers { get; set; }

        [Required]
        [Display(Name = "Customer")]
        public int SelectedCustomer { get; set; }
    }
}