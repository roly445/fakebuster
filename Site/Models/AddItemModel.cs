namespace WebsiteTest.Site.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class AddItemModel : BaseModel
    {
        public AddItemModel()
        {
            StockTypes = new List<SelectListItem>();
        }

        [Required]
        public string Title { get; set; }

        public List<SelectListItem> StockTypes { get; set; }

        [Required]
        [Display(Name = "Stock Type")]
        public int SelectedStockType { get; set; }
    }
}