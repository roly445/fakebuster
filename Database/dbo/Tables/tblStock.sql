﻿CREATE TABLE [dbo].[tblStock]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Title] VARCHAR(255) NOT NULL, 
    [StockTypeId] INT NOT NULL
)
