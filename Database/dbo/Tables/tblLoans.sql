﻿CREATE TABLE [dbo].[tblLoans]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CustomerId] INT NOT NULL, 
    [StockId] INT NOT NULL, 
    [LoanedOut] DATETIME NOT NULL, 
    [Returned] DATETIME NULL
)
