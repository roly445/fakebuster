﻿namespace WebsiteTest.Repository
{
    using System.Data.Entity;
    using System.Diagnostics;
    using Domain.Entities;

    public class RepoDbContext : DbContext
    {
        public RepoDbContext()
                : base("connStr")
        {
            Debug.Write(Database.Connection.ConnectionString);
            //Database.SetInitializer<RepoDbContext>(new DatabaseInitializer());
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Loan> Loans { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<StockType> StockTypes { get; set; }

        #region Methods

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<RepoDbContext>(null);
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Customer>().ToTable("tblCustomers");

            modelBuilder.Entity<Loan>().ToTable("tblLoans");
            modelBuilder.Entity<Loan>()
                    .HasRequired(l => l.Stock)
                    .WithMany(s => s.Loans)
                    .HasForeignKey(l => l.StockId);
            modelBuilder.Entity<Loan>()
                    .HasRequired(l => l.Customer)
                    .WithMany(c => c.Loans)
                    .HasForeignKey(l => l.CustomerId);

            modelBuilder.Entity<Stock>().ToTable("tblStock");
            modelBuilder.Entity<Stock>()
                    .HasRequired(s => s.StockType)
                    .WithMany(sT => sT.Stocks)
                    .HasForeignKey(s => s.StockTypeId);

            modelBuilder.Entity<StockType>().ToTable("tblStockType");

            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Customer>().ToTable("tblCustomers");

            //#region Checkin

            //modelBuilder.Entity<Checkin>()
            //            .HasRequired(t => t.Trek)
            //            .WithMany(t => t.Checkins)
            //            .HasForeignKey(d => d.TrekId);
            //modelBuilder.Entity<Checkin>()
            //            .HasOptional(t => t.UploadedImage)
            //            .WithMany(t => t.Checkins)
            //            .HasForeignKey(d => d.UploadedImageId);
            //modelBuilder.Entity<Checkin>()
            //            .HasRequired(t => t.User)
            //            .WithMany(t => t.Checkins)
            //            .HasForeignKey(d => d.UserId);

            //#endregion

            //#region Group

            //modelBuilder.Entity<Group>()
            //            .HasMany(t => t.Members)
            //            .WithMany(t => t.Groups)
            //            .Map(
            //                m =>
            //                {
            //                    m.ToTable("UsersInGroup");
            //                    m.MapLeftKey("GroupId");
            //                    m.MapRightKey("UserId");
            //                });

            //modelBuilder.Entity<Group>()
            //            .HasMany(t => t.Administrators)
            //            .WithMany(t => t.GroupsAdmined)
            //            .Map(
            //                m =>
            //                {
            //                    m.ToTable("GroupAdmin");
            //                    m.MapLeftKey("GroupId");
            //                    m.MapRightKey("UserId");
            //                });

            //modelBuilder.Entity<Group>()
            //            .HasRequired(t => t.Profile)
            //            .WithMany(t => t.Groups)
            //            .HasForeignKey(d => d.ProfileId);

            //modelBuilder.Entity<Group>()
            //            .Property(g => g.CreatedById).HasColumnName("CreatedBy");
            //modelBuilder.Entity<Group>()
            //            .HasRequired(g => g.CreatedBy)
            //            .WithMany(u => u.GroupsCreated)
            //            .HasForeignKey(g => g.CreatedById);

            //#endregion

            //#region Organisation

            //modelBuilder.Entity<Organisation>()
            //            .HasMany(t => t.Members)
            //            .WithMany(t => t.Organisations)
            //            .Map(
            //                m =>
            //                {
            //                    m.ToTable("UsersInOrganisation");
            //                    m.MapLeftKey("OrganistaionId");
            //                    m.MapRightKey("UserId");
            //                });

            //modelBuilder.Entity<Organisation>()
            //            .HasMany(t => t.Administrators)
            //            .WithMany(t => t.OrganisationsAdmined)
            //            .Map(
            //                m =>
            //                {
            //                    m.ToTable("OrganisationAdmin");
            //                    m.MapLeftKey("OrganisationId");
            //                    m.MapRightKey("UserId");
            //                });

            //modelBuilder.Entity<Organisation>()
            //            .HasRequired(t => t.Profile)
            //            .WithMany(t => t.Organisations)
            //            .HasForeignKey(d => d.ProfileId);

            //modelBuilder.Entity<Organisation>()
            //            .Property(g => g.CreatedById).HasColumnName("CreatedBy");
            //modelBuilder.Entity<Organisation>()
            //            .HasRequired(g => g.CreatedBy)
            //            .WithMany(u => u.OrganisationsCreated)
            //            .HasForeignKey(g => g.CreatedById);

            //#endregion

            //#region Profile

            //modelBuilder.Entity<Profile>()
            //            .HasOptional(t => t.UploadedImage)
            //            .WithMany(t => t.Profiles)
            //            .HasForeignKey(d => d.UploadedImageId);

            //#endregion

            //#region Trek

            //modelBuilder.Entity<Trek>()
            //            .HasMany(t => t.Followers)
            //            .WithMany(t => t.Following)
            //            .Map(
            //                m =>
            //                {
            //                    m.ToTable("FavouriteTrek");
            //                    m.MapLeftKey("TrekId");
            //                    m.MapRightKey("UserId");
            //                });
            //modelBuilder.Entity<Trek>()
            //            .HasMany(t => t.Participants)
            //            .WithMany(t => t.Participanting)
            //            .Map(
            //                m =>
            //                {
            //                    m.ToTable("TrekParticipant");
            //                    m.MapLeftKey("TrekId");
            //                    m.MapRightKey("UserId");
            //                });
            //modelBuilder.Entity<Trek>()
            //            .HasRequired(t => t.Profile)
            //            .WithMany(t => t.Treks)
            //            .HasForeignKey(d => d.ProfileId);

            //#endregion

            //#region User

            //modelBuilder.Entity<User>()
            //            .HasRequired(t => t.Legal)
            //            .WithMany(t => t.Users)
            //            .HasForeignKey(d => d.LegalId);
            //modelBuilder.Entity<User>()
            //            .HasRequired(t => t.Profile)
            //            .WithMany(t => t.Users)
            //            .HasForeignKey(d => d.ProfileId);

            //#endregion

            //#region Waypoint

            //modelBuilder.Entity<Waypoint>()
            //            .HasRequired(t => t.Trek)
            //            .WithMany(t => t.Waypoints)
            //            .HasForeignKey(d => d.TrekId);

            //#endregion
        }

        #endregion
    }
}