﻿namespace WebsiteTest.Repository
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using Domain.Entities;

    public class DatabaseInitializer : CreateDatabaseIfNotExists<RepoDbContext>
    {
        protected override void Seed(RepoDbContext context)
        {
            List<Customer> customers = new List<Customer>
                                       {
                                               new Customer {Id = 1, FirstName = "Jenny", LastName = "Smith"},
                                               new Customer {Id = 2, FirstName = "Terry", LastName = "Smith"},
                                               new Customer {Id = 3, FirstName = "Jane", LastName = "Brown"},
                                               new Customer {Id = 4, FirstName = "Trevor", LastName = "Green"},
                                               new Customer {Id = 5, FirstName = "John", LastName = "Bean"},
                                               new Customer {Id = 6, FirstName = "Sarah", LastName = "Daniels"},
                                               new Customer {Id = 7, FirstName = "Gary", LastName = "Waters"},
                                               new Customer {Id = 8, FirstName = "Gavin", LastName = "Rockwell"},
                                               new Customer {Id = 9, FirstName = "Joanne", LastName = "Wilson"},
                                       };

            customers.ForEach(s => context.Customers.Add(s));
            context.SaveChanges();
        }
    }
}